Git:
Git is a software basically designed for coordinating work among programmers, it can also be used to track changes in any set of files. Its goals include speed, data integrity and support for distributed, non-linear workflows.
Git is a system that helps in maintaining and creating a software. It is very useful when multiple people wants to work on single project. Basically , everyone can create a copy of pre implemented code and work on different parts and then everyone just merges it together on git to give completely working end to end project.

Git is comprises of various elements i.e. Repository,  Gitlab, Commit, Push, Branch, Merge, Clone, Fork.

Git has three main states : modified, staged, and committed :
1.Modified means that you have changed the file but have not committed it to your repo yet.
2.Staged means that you have marked a modified file in its current version to go into your next picture.
3.Committed means that the data is safely stored in your local repo in form of pictures.

Trees:
1.Workspace : It�s the editor of your repo.
2.Staging : All the staged files go into this tree of your repo.
3.Local Repository : All the committed files goes here.
4.Remote Repository : This is the copy of your Local Repository but is stored in some server on the Internet. 

Git Workflow:
1.Clone the repo.
2.Create new branch
3.Modify files in your working tree. 
4.Commit your work.
5.Lastly, push it.

